package com.example.cupcake

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.cupcake.databinding.FragmentCustomerDetailsBinding
import com.example.cupcake.model.OrderViewModel

class CustomerDetailsFragment : Fragment() {

    private var binding: FragmentCustomerDetailsBinding? = null

    private val sharedViewModel: OrderViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding =
            FragmentCustomerDetailsBinding.inflate(layoutInflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.apply {
            customerDetailFragment = this@CustomerDetailsFragment
            viewModel = sharedViewModel
            lifecycleOwner = viewLifecycleOwner
        }
    }

    fun goToNextScreen() {
        Log.e("goToNextScreen", binding?.nameEditText?.text.toString())
        if (binding?.nameEditText?.text?.toString()?.equals("") == true) {
            binding?.name?.error = "Input your name here..."
            return
        }
        sharedViewModel.setName(binding?.nameEditText?.text.toString())
        findNavController().navigate(R.id.action_customerDetailsFragment_to_summaryFragment)
    }

    fun cancelOrder() {
        sharedViewModel.resetOrder()
        findNavController().navigate(R.id.action_customerDetailsFragment_to_startFragment)
    }
}