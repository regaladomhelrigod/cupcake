package com.example.cupcake.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

private const val PRICE_PER_CUPCAKE = 2.00
private const val PRICE_FOR_SAME_DAY_PICKUP = 3.00

class OrderViewModel : ViewModel() {
    private val _quantity = MutableLiveData<Int>()
    val quantity: LiveData<Int> = _quantity

    private val _flavor = MutableLiveData<ArrayList<String>>()
    val flavor: LiveData<ArrayList<String>> = _flavor
    var flavorTransformed: LiveData<String> = Transformations.map(_flavor){
        it.joinToString(", ")
    }

    private val _date = MutableLiveData<String>()
    val date: LiveData<String> = _date

    private val _name = MutableLiveData<String>()
    val name: LiveData<String> = _name

    private val _allowMultipleFlavor = MutableLiveData<Boolean>()
    val allowMultipleFlavor: LiveData<Boolean> = _allowMultipleFlavor

    private val _price = MutableLiveData<Double>()
    val price: LiveData<String> = Transformations.map(_price) {
        NumberFormat.getCurrencyInstance().format(it)
    }

    val dateOptions = getPickupOptions()

    init {
        resetOrder()
    }

    fun setQuantity(numberCupcakes: Int) {
        if (numberCupcakes > 1){
            hasMoreThanOneOrder(true)
        }

        _quantity.value = numberCupcakes
        updatePrice()
    }

    fun addFlavor(desiredFlavor: String) {
        _flavor.value?.add(desiredFlavor)
        Log.e("setFlavor", _flavor.value.toString());
    }

    fun removeFlavor(flavorToBeRemoved: String){
        _flavor.value?.remove(flavorToBeRemoved)
        Log.e("setFlavor", _flavor.value.toString());
    }

    fun setDate(pickupDate: String) {
        _date.value = pickupDate
        updatePrice()
    }

    fun setName(name: String?) {
        _name.value = name
        Log.e("setName", "setName : $name")
    }

    private fun hasMoreThanOneOrder(moreThanOne: Boolean){
        _allowMultipleFlavor.value = moreThanOne
    }

    fun hasNoFlavorSet(): Boolean {
        return _flavor.value.isNullOrEmpty()
    }

    private fun getPickupOptions(): List<String> {
        val options = mutableListOf<String>()
        val formatter = SimpleDateFormat("E MMM d", Locale.getDefault())
        val calendar = Calendar.getInstance()

        repeat(4) {
            options.add(formatter.format(calendar.time))
            calendar.add(Calendar.DATE, 1)
        }

        return options
    }

    fun resetOrder() {
        _quantity.value = 0
        _flavor.value = arrayListOf()
        _date.value = dateOptions[0]
        _price.value = 0.0
        _name.value = ""
        flavorTransformed = Transformations.map(_flavor){
            it.joinToString(", ")
        }
    }

    private fun updatePrice() {
        _price.value = (quantity.value ?: 0) * PRICE_PER_CUPCAKE

        if (dateOptions[0] == _date.value) {
            _price.value = (_price.value)?.plus(PRICE_FOR_SAME_DAY_PICKUP)
        }
    }

    fun notAvailableForSameDayPickup(flavor: String): Boolean {
        return _flavor.value?.contains(flavor) != true
    }
}